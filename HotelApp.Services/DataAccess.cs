﻿using HotelApp.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace HotelApp.Services
{
    public class DataAccess
    {
        private readonly string _connectionString;
        private readonly string _providerName;
        private readonly DbProviderFactory _providerFactory;

        public DataAccess()
        {
            _connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            _providerName = ConfigurationManager.ConnectionStrings["ConnectionString"].ProviderName;
            _providerFactory = DbProviderFactories.GetFactory(_providerName);
        }

        public List<User> GetAllUsers()
        {
            var dataUsers = new List<User>();

            using (var connection = _providerFactory.CreateConnection())
            using (var command = connection.CreateCommand())
            {
                try
                {
                    connection.ConnectionString = _connectionString;
                    connection.Open();

                    command.CommandText = "select * from Users";

                    var dataReader = command.ExecuteReader();

                    while (dataReader.Read())
                    {
                        var number = dataReader["Number"].ToString();
                        var firstName = dataReader["FirstName"].ToString();
                        var lastName = dataReader["LastName"].ToString();

                        dataUsers.Add(new User
                        {
                            Number = number,
                            FirstName = firstName,
                            LastName = lastName
                        });
                    }

                    dataReader.Close();
                }
                catch (DbException exception)
                {
                    //TODO обработка ошибки
                    throw;
                }
                catch (Exception exception)
                {
                    //TODO обработка ошибки
                    throw;
                }
            }
            return dataUsers;
        }

        public User GetUserByNumber(string number)
        {

            User user = new User();
            if (!Regex.IsMatch(number, @"\(?\d{3}\)?-? *\d{3}-? *-?\d{4}"))
            {
                Console.WriteLine("Must be number in format:+77775552211");
                user.Number = "!";
                return user;
            }
            else
            {
                using (var connection = _providerFactory.CreateConnection())
                using (var command = connection.CreateCommand())
                {
                    try
                    {
                        connection.ConnectionString = _connectionString;
                        connection.Open();
                        command.CommandText = $"select * from Users where number={number.ToString()}";

                        var dataReader = command.ExecuteReader();

                        while (dataReader.Read())
                        {
                            var tmpNumber = dataReader["Number"].ToString();
                            var firstName = dataReader["FirstName"].ToString();
                            var lastName = dataReader["LastName"].ToString();
                            var id = dataReader["Id"];
                            var password = dataReader["password"].ToString();

                            user.Id = (int)id;
                            user.FirstName = firstName;
                            user.LastName = lastName;
                            user.Number = tmpNumber;
                            user.Password = password;
                        }

                        dataReader.Close();
                    }
                    catch (DbException exception)
                    {
                        //TODO обработка ошибки
                        throw;
                    }
                    catch (Exception exception)
                    {
                        //TODO обработка ошибки
                        throw;
                    }
                }
                return user;
            }
        }


        public List<Hotels> GetAllHotels()
        {
            var dataHotels = new List<Hotels>();
            using (var connection = _providerFactory.CreateConnection())
            using (var command = connection.CreateCommand())
            {
                try
                {
                    connection.ConnectionString = _connectionString;
                    connection.Open();

                    command.CommandText = "select * from Hotels";

                    var dataReader = command.ExecuteReader();

                    while (dataReader.Read())
                    {
                        var hotelId = (int)dataReader["Id"];
                        var hotelName = dataReader["HotelName"].ToString();
                        var hotelCity = dataReader["HotelCity"].ToString();
                        var hotelStar = (int)dataReader["HotelStar"];
                        var hotelPriceForDay = (int)dataReader["HotelPriceForDay"];
                        var hotelRooms = (int)dataReader["HotelRooms"];
                        var hotelFreeRooms = (int)dataReader["HotelFreeRooms"];


                        dataHotels.Add(new Hotels
                        {
                            HotelId = hotelId,
                            HotelName = hotelName,
                            HotelCity = hotelCity,
                            HotelStar = hotelStar,
                            HotelPriceForDay = hotelPriceForDay,
                            HotelFreeRooms = hotelPriceForDay
                        });
                    }

                    dataReader.Close();
                }
                catch (DbException exception)
                {
                    //TODO обработка ошибки
                    throw;
                }
                catch (Exception exception)
                {
                    //TODO обработка ошибки
                    throw;
                }
            }
            return dataHotels;

        }

        public void AddUser(User user)
        {
            using (var connection = _providerFactory.CreateConnection())
            using (var command = connection.CreateCommand())
            {
                DbTransaction transaction = null;
                try
                {
                    connection.ConnectionString = _connectionString;
                    connection.Open();

                    transaction = connection.BeginTransaction();

                    command.CommandText = $"insert into Users (Number, Password, FirstName, LastName) values(@number, @password, @firstName, @lastName)";
                    command.Transaction = transaction;

                    DbParameter numberParameter = command.CreateParameter();
                    numberParameter.ParameterName = "@number";
                    numberParameter.Value = user.Number;
                    numberParameter.DbType = System.Data.DbType.String;
                    numberParameter.IsNullable = false;



                    DbParameter passwordParameter = command.CreateParameter();
                    passwordParameter.ParameterName = "@password";
                    passwordParameter.Value = user.Password;
                    passwordParameter.DbType = System.Data.DbType.String;
                    passwordParameter.IsNullable = false;


                    DbParameter firstNameParameter = command.CreateParameter();
                    firstNameParameter.ParameterName = "@firstname";
                    firstNameParameter.Value = user.FirstName;
                    firstNameParameter.DbType = System.Data.DbType.String;
                    firstNameParameter.IsNullable = false;


                    DbParameter lastNameParameter = command.CreateParameter();
                    lastNameParameter.ParameterName = "@lastname";
                    lastNameParameter.Value = user.LastName;
                    lastNameParameter.DbType = System.Data.DbType.String;
                    lastNameParameter.IsNullable = false;


                    command.Parameters.AddRange(new DbParameter[] { numberParameter, passwordParameter, firstNameParameter, lastNameParameter });

                    var affectedRows = command.ExecuteNonQuery();

                    if (affectedRows < 1)
                    {
                        throw new Exception("Insert not done");
                    }
                    transaction.Commit();
                    transaction.Dispose();
                }
                catch (DbException exception)
                {

                    //TODO обработка ошибки
                    transaction?.Rollback();
                    transaction.Dispose();
                    throw;
                }
                catch (Exception exception)
                {
                    transaction?.Rollback();
                    transaction.Dispose();
                    //TODO обработка ошибки
                    throw;
                }
            }
        }

        public void AddOrder(User user)
        {
            Console.WriteLine("Enter booking year");
            int.TryParse(Console.ReadLine(), out int year);
            Console.WriteLine("Enter booking month");
            int.TryParse(Console.ReadLine(), out int month);
            Console.WriteLine("Enter booking day");
            int.TryParse(Console.ReadLine(), out int day);


            DateTime  bookingDate = new DateTime(year, month, day);
            DateTime orderDate = DateTime.Now;

            Console.WriteLine("Enter Hotel Id");
            int.TryParse(Console.ReadLine(), out int hotelId);
            using (var connection = _providerFactory.CreateConnection())
            using (var command = connection.CreateCommand())
            {
                DbTransaction transaction = null;
                try
                {
                    connection.ConnectionString = _connectionString;
                    connection.Open();

                    transaction = connection.BeginTransaction();

                    command.CommandText = $"insert into Orders (UserId, HotelId, OrderDate, BookingDate) values(@userId, @hotelId, @orderDate, @bookingDate)";
                    command.Transaction = transaction;

                    DbParameter userIdParameter = command.CreateParameter();
                    userIdParameter.ParameterName = "@userId";
                    userIdParameter.Value = user.Id;
                    userIdParameter.DbType = System.Data.DbType.Int32;
                    userIdParameter.IsNullable = false;



                    DbParameter hotelIdParameter = command.CreateParameter();
                    hotelIdParameter.ParameterName = "@hotelId";
                    hotelIdParameter.Value = hotelId;
                    hotelIdParameter.DbType = System.Data.DbType.Int32;
                    hotelIdParameter.IsNullable = false;


                    DbParameter orderDateParameter = command.CreateParameter();
                    orderDateParameter.ParameterName = "@orderDate";
                    orderDateParameter.Value = orderDate.ToString();
                    orderDateParameter.DbType = System.Data.DbType.String;
                    orderDateParameter.IsNullable = false;


                    DbParameter bookingDateParameter = command.CreateParameter();
                    bookingDateParameter.ParameterName = "@bookingDate";
                    bookingDateParameter.Value = bookingDate.ToString();
                    bookingDateParameter.DbType = System.Data.DbType.String;
                    bookingDateParameter.IsNullable = false;


                    command.Parameters.AddRange(new DbParameter[] { userIdParameter, hotelIdParameter, orderDateParameter, bookingDateParameter });

                    var affectedRows = command.ExecuteNonQuery();

                    if (affectedRows < 1)
                    {
                        throw new Exception("Insert not done");
                    }
                    transaction.Commit();
                    transaction.Dispose();
                }
                catch (DbException exception)
                {

                    //TODO обработка ошибки
                    transaction?.Rollback();
                    transaction.Dispose();
                    throw;
                }
                catch (Exception exception)
                {
                    transaction?.Rollback();
                    transaction.Dispose();
                    //TODO обработка ошибки
                    throw;
                }
            }
        }

        public void DeleteUserById(int id)
        {

        }

        public void UpdateUser(User user)
        {

        }
    }

}
