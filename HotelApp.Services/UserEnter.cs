﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;
using Twilio.TwiML;

using HotelApp.Models;
using System.Text.RegularExpressions;

namespace HotelApp.Services
{
    public class UserEnter
    {
        User user = new User
        {
            FirstName = "",
            LastName = "",
            Number = "",
            Password = "",
            DoublePassword = ""

        };

        public void ValidationCheck()
        {

            int lengthPassword = user.Password.Length;
            int minimalLengthOfPassword = 8;

            Console.WriteLine("Enter first name: ");
            user.FirstName = Console.ReadLine();
            try
            {
                if (!Regex.IsMatch(user.FirstName, @"^[\p{L}]+$"))
                {

                    throw new ArgumentException();
                }
            }
            catch (ArgumentException)
            {
                Console.WriteLine("First Name Contains is unacceptable symbols");
                throw new ArgumentException();
            }

            Console.WriteLine("Enter last name: ");
            user.LastName = Console.ReadLine();
            try
            {
                if (!Regex.IsMatch(user.LastName, @"^[\p{L}]+$"))
                {
                    throw new ArgumentException();
                }
            }
            catch (ArgumentException)
            {
                Console.WriteLine("Last Name Contains is unacceptable symbols");
                throw new ArgumentException();
            }

            Console.WriteLine("Enter Password: ");
            user.Password = ReadPassword();
            try
            {
                if (user.Password.Length < minimalLengthOfPassword)
                {
                    throw new ArgumentException();
                }
            }
            catch (ArgumentException)
            {
                Console.WriteLine("Password does not meet the requirements");
                throw new ArgumentException();
            }
            try
            {
                if (!user.Password.Contains('!') && !user.Password.Contains('$') && !user.Password.Contains('%') && !user.Password.Contains(':') && !user.Password.Contains('?') && !user.Password.Contains('&') && !user.Password.Contains('*') && !user.Password.Contains('(') && !user.Password.Contains(')') && !user.Password.Contains('+') && !user.Password.Contains('-') && !user.Password.Contains(']') && !user.Password.Contains('[') && !user.Password.Contains('\'') && !user.Password.Contains('/') && !user.Password.Contains('.') && !user.Password.Contains('?') && !user.Password.Contains('<') && !user.Password.Contains('>'))
                {
                    throw new ArgumentException();
                }
            }
            catch (ArgumentException)
            {
                Console.WriteLine("Password does not meet the requirements");
                throw new ArgumentException();
            }
            try
            {
                if (!user.Password.Any(Char.IsNumber) || !user.Password.Any(Char.IsUpper) || !user.Password.Any(Char.IsLower))
                {
                    throw new ArgumentException();
                }
            }
            catch (ArgumentException)
            {
                Console.WriteLine("Password does not meet the requirements");
                throw new ArgumentException();
            }
            Console.WriteLine("Retype Password: ");
            user.DoublePassword = ReadPassword();
            try
            {
                if (user.DoublePassword != user.Password)
                {
                    throw new ArgumentException();
                }
            }
            catch (ArgumentException)
            {
                Console.WriteLine("Passwords did not match");
                throw new ArgumentException();
            }

            Console.WriteLine("Enter phone number: ");
            user.Number = Console.ReadLine();
            try
            {
                if (string.IsNullOrEmpty(user.Number) || user.Number.Length > 12 || user.Number.Contains('-') || user.Number.Contains(' '))
                {
                    throw new ArgumentException();
                }
            }
            catch (ArgumentException)
            {
                throw new ArgumentException("Phone number is not correct");
                throw new ArgumentException();
            }

        }

        public static string ReadPassword()
        {
            string password = "";
            ConsoleKeyInfo info = Console.ReadKey(true);
            while (info.Key != ConsoleKey.Enter)
            {
                if (info.Key != ConsoleKey.Backspace)
                {
                    Console.Write("*");
                    password += info.KeyChar;
                }
                else if (info.Key == ConsoleKey.Backspace)
                {
                    if (!string.IsNullOrEmpty(password))
                    {

                        password = password.Substring(0, password.Length - 1);
                        int pos = Console.CursorLeft;
                        Console.SetCursorPosition(pos - 1, Console.CursorTop);
                        Console.Write(" ");
                        Console.SetCursorPosition(pos - 1, Console.CursorTop);
                    }
                }
                info = Console.ReadKey(true);
            }
            Console.WriteLine();
            return password;
        }

        public bool SendSms()
        {
            Random rand = new Random();
            var accountSid = "ACa0ae7451804ec0451cd0a5630c574985";
            var authToken = "242ff845bc90e6a2538318c9da69a0bc";
            string code = Convert.ToString(rand.Next(1000, 9999));
            TwilioClient.Init(accountSid, authToken);
            var to = new PhoneNumber(user.Number);
            var from = new PhoneNumber("+17025701146");
            var message = MessageResource.Create(
                to: to,
                from: from,
                body: code);
            Console.WriteLine(message.Body);
            Console.WriteLine("Enter your sms code: ");
            string checkCode = Console.ReadLine();
            if (checkCode == code)
            {
                Console.WriteLine("Registration complited successfully!");
                return true;
            }
            else
            {
                Console.WriteLine("Invalid SMS code! Registration failed!");
                return false;
            }
        }

        public User ExportUser()
        {
            return user;
        }

    }
}
