﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HotelApp.Services;
using System.Threading.Tasks;
using HotelApp.Models;

namespace ado.net_HotelApp
{
    class Program
    {
        static void Main(string[] args)
        {
            DataAccess dataAccess = new DataAccess();

            string menu = "";
            Console.WriteLine("Press 1 to Log in");
            Console.WriteLine("Press 2 to Register");

            menu = Console.ReadLine();
            switch (menu)
            {
                case "1":

                    Console.WriteLine("Enter phone number");
                    string number = Console.ReadLine();
                    Console.WriteLine("Enter password");
                    string password = UserEnter.ReadPassword();

                    if (dataAccess.GetUserByNumber(number).Number != null && dataAccess.GetUserByNumber(number).Password == password)
                    {

                        Console.WriteLine("Press 1 to View List of Hotels");
                        Console.WriteLine("Press 2 to Make Order");
                        string subMenu = Console.ReadLine();
                        switch (subMenu)
                        {
                            case "1":
                                foreach (var item in dataAccess.GetAllHotels())
                                {
                                    Console.WriteLine();
                                    Console.WriteLine("-------------------------------------------");
                                    Console.WriteLine($"Hotel Id is: \t\t\t{ item.HotelId}");
                                    Console.WriteLine($"Hotel City is: \t\t\t{ item.HotelCity}");
                                    Console.WriteLine($"Hotel Free Rooms count are: \t\t{ item.HotelFreeRooms}");
                                    Console.WriteLine($"Hotel Name is: \t\t\t{ item.HotelName}");
                                    Console.WriteLine($"Hotel Price for a day is: \t\t{ item.HotelPriceForDay}");
                                    Console.WriteLine($"Hotel Rooms is: \t\t\t{ item.HotelRooms}");
                                    Console.WriteLine($"Hotel Stars are: \t\t\t{ item.HotelStar}");
                                    Console.WriteLine("-------------------------------------------");
                                    Console.WriteLine();
                                }
                                break;

                            case "2":
                                dataAccess.AddOrder(dataAccess.GetUserByNumber(number));
                                break;
                            default:
                                break;
                        }
                        break;
                    }
                    else if (dataAccess.GetUserByNumber(number).Number == "!")
                    {
                        break;
                    }
                    else Console.WriteLine("user not registered or password incorrect");
                    break;
                case "2":

                    UserEnter userEnter = new UserEnter();
                    userEnter.ValidationCheck();
                    if (userEnter.SendSms())
                    {
                        dataAccess.AddUser(userEnter.ExportUser());
                    }
                    break;

                default:
                    break;
            }

            Console.ReadLine();
        }
    }
}
